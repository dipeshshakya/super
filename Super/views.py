from django.shortcuts import render
from django.views.generic import TemplateView
from product.models import Product,Category

class HomeView(TemplateView):
    template_name = 'index.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data( **kwargs)
        context['product_list'] = Product.objects.all()[:10]  #.filter(category__name='first parent')[:10]
        context['category_list'] = Category.objects.all()
        return context