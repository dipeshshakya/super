from django.db import models
from django.contrib.auth.models import User
from account.models import Account
from product.models import Product
from django.db.models import Sum,Avg
# Create your models here.


RATING_CHOICES = [
    (0,0),
    (1,1),
    (2,2),
    (3,3),
    (4,4),
    (5,5),
]


class Comments(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    created_on = models.DateTimeField(auto_now_add = True)
    modified_on = models.DateTimeField(auto_now_add = True)
    comments = models.TextField(max_length = 500, null = True, blank = True)
    rating = models.IntegerField(choices = RATING_CHOICES, default = 0)



    @staticmethod
    def average_rating(self):
        all_comments=Comments.objects.filter(rating__gt=0)
      #  total_rating=all_comments.annonate(total=Sum('rating'))
        avg_rating=all_comments.aggregate(Avg('rating'))
        return avg_rating['rating__avg']