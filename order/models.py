from django.db import models
from django.contrib.auth.models import User
from product.models import Product,Category,Location
from cart.models import Cart
# Create your models here.
class Order(models.Model):
	user = models.ForeignKey(User)
	sub_total = models.DecimalField(decimal_places=2, max_digits=11)

	tax = models.DecimalField(default=0.13,decimal_places=2,max_digits=10)
	shippingcharge =models.IntegerField(default=0)
	discount = models.DecimalField(default=0,decimal_places=2,max_digits=11)
	total = models.DecimalField(decimal_places=2, max_digits=11)
	

class OrderItem(models.Model):
	order = models.ForeignKey(Order)
	name = models.CharField(max_length=255, blank=False, null=False)    
	price = models.DecimalField(decimal_places=2, max_digits=11)
	qty = models.IntegerField(default=1)
	manufacturer = models.CharField(max_length=20,blank=True)
	created_on = models.DateTimeField(auto_now_add = True)
	modified_on = models.DateTimeField(auto_now_add = True)


	#def __str__(self):
    #	return "%s %s %s" %(self.name, self.price, self.qty)
class BillTo(models.Model):
	order = models.ForeignKey(Order)
	company_name = models.CharField(max_length=50, blank=False,null=False)
	email = models.CharField(max_length=50,blank=False,null=False)
	title = models.CharField(max_length=20)
	firstname = models.CharField(max_length=15,blank=False,null=False)
	middlename = models.CharField(max_length=15,blank=True,null=True)
	lastname = models.CharField(max_length=15,blank=False,null=False)
	address1 = models.CharField(max_length=15,blank=False,null=False)
	address2 = models.CharField(max_length=15,blank=True,null=True)
	postalcode = models.IntegerField(blank=False,null=False)
	state = models.CharField(max_length=20,blank=False,null=False)
	phone = models.IntegerField(blank=False,null=False)
	mobile = models.IntegerField(blank=True,null=True)
	msg = models.CharField(max_length=140,blank=False,null=False)