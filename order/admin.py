from django.contrib import admin
from .models import Order,OrderItem,BillTo
# Register your models here.
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0
class BillToInline(admin.StackedInline):
    model = BillTo  
    extra = 0 

class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemInline,BillToInline]
    
admin.site.register(Order,OrderAdmin)
