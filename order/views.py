from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response
from django.views.generic import TemplateView
from django.db import transaction
from django.shortcuts import redirect
from .models import Order,BillTo,OrderItem
from cart.models import Cart

class OrderView(TemplateView):
	template_name="checkout.html"
	model = Cart

	def get_context_data(self, **kwargs):
		context = super(OrderView, self).get_context_data(**kwargs)
		cart = Cart.objects.filter(user=self.request.user)
		context['cart_list'] = cart
		sub_total = 0
		for c in cart:
		    sub_total += c.product.price * c.quantity
		context['sub_total'] = sub_total
		return context
	

	def post(self, request, *args, **kwargs):
		company_name = request.POST['company_name']
		email = request.POST['email']
		title = request.POST['title']
		firstname = request.POST['firstname']
		middlename = request.POST['middlename']
		lastname = request.POST['lastname']
		address1 = request.POST['address1']
		address2 = request.POST['address2']
		postalcode = request.POST['postalcode']
		state = request.POST['state']
		phone = request.POST['phone']
		mobile = request.POST['mobile']
		msg = request.POST['msg']
		print company_name,email,title,firstname,lastname,middlename,address2,address1,postalcode,state,phone,msg,mobile
	#	return render(request, self.template_name, {'form_data': request.POST})
		
		

		tax = 0
		discount = 0 
		shippingcharge = 0

		current_cart_list = Cart.objects.filter(user=self.request.user)
		sub_total = 0
		for c in current_cart_list:
		    sub_total += c.product.price * c.quantity

		# order saving starts from here ....
		with transaction.atomic():
			# first step order model save
			# second step order item save
			# third step billing adddress save
			order = Order(
				user= request.user,
				tax=tax,
				discount=discount,
				shippingcharge = shippingcharge,
				sub_total=sub_total,
				total = sub_total,
								# need to save payment method, total amount
				)
			order.save()

			#second steps
			for item in  current_cart_list:
				oitem = OrderItem(
					name = item.product.name, 
					price = item.product.price, 
					manufacturer = item.product.manufacturer,
					qty = item.quantity,
					order = order
					)
				oitem.save()

			# delete current cart items7
			current_cart_list.delete()

			# third steps 
			bill_to = BillTo(
				company_name = company_name, 
				email = email, 
				title = title, 
				firstname = firstname, 
				middlename = middlename, 
				lastname = lastname,
				address1 = address1,
				address2 = address2, 
				postalcode = postalcode, 
				state= state, 
				phone = phone, 
				mobile = mobile, 
				msg = msg,
				order = order,
				#model=view POST  ko variable
				)
			bill_to.save()

		# send to thanks page or redirect to payment page
		#return redirect('/')


		from paypal.standard.forms import PayPalPaymentsForm
		from django.conf import settings
		from django.core.urlresolvers import reverse
		paypal_dict = {
			"business": settings.PAYPAL_RECEIVER_EMAIL,
			"amount": sub_total,
			"item_name": "Item name",
			"invoice": "order-%s" %(order.id),
			"notify_url": "https://www.example.com" + reverse('paypal-ipn'),
			"return_url": "https://www.example.com/your-return-location/",
			"cancel_return": "https://www.example.com/your-cancel-location/",
			"custom": "Upgrade all users!",  # Custom command to correlate to some function later (optional)
		}

		# Create the instance.
		form = PayPalPaymentsForm(initial=paypal_dict)
		context = {"form": form}
		return render(request, "payment.html", context)


		return render(request, self.template_name, {'form_data': request.POST})
