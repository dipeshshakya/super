# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BillTo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company_name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('title', models.CharField(max_length=20)),
                ('firstname', models.CharField(max_length=15)),
                ('middlename', models.CharField(max_length=15, null=True, blank=True)),
                ('lastname', models.CharField(max_length=15)),
                ('address1', models.CharField(max_length=15)),
                ('address2', models.CharField(max_length=15, null=True, blank=True)),
                ('postal_code', models.IntegerField()),
                ('country', models.CharField(max_length=20)),
                ('phone', models.IntegerField(max_length=10)),
                ('order', models.ForeignKey(to='order.Order')),
            ],
        ),
    ]
