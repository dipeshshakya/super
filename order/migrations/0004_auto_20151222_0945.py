# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_auto_20151220_0406'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='discount',
            field=models.DecimalField(default=0, max_digits=11, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='order',
            name='shippingcharge',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='order',
            name='tax',
            field=models.DecimalField(default=0.13, max_digits=10, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='manufacturer',
            field=models.CharField(max_length=20, blank=True),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='qty',
            field=models.IntegerField(default=1),
        ),
    ]
