# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_billto'),
    ]

    operations = [
        migrations.RenameField(
            model_name='billto',
            old_name='postal_code',
            new_name='postalcode',
        ),
        migrations.RenameField(
            model_name='billto',
            old_name='country',
            new_name='state',
        ),
        migrations.AddField(
            model_name='billto',
            name='mobile',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='billto',
            name='msg',
            field=models.CharField(default=2, max_length=140),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='billto',
            name='phone',
            field=models.IntegerField(),
        ),
    ]
