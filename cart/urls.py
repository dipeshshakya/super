from django.conf.urls import include,url,patterns
from .views import CartView,ajaxCartForm
urlpatterns = patterns(
	"",
	url(r'^$', CartView.as_view(),name="cart"),
	url(r'^ajax/cart/$', ajaxCartForm),
)