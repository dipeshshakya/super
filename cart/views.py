from django.shortcuts import render


# Create your views here.
from django.views.generic import ListView
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.db.models import Sum,Avg

import json
from product.models import Product
from .models import Cart

class CartView(ListView):
    template_name = 'cart/cart.html'
    
    def get_queryset(self):
        return Cart.objects.filter(user=self.request.user)
    
    
    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        cart = Cart.objects.filter(user=self.request.user)
      
        sub_total = 0
        for c in cart:
            sub_total += c.product.price * c.quantity
        context['sub_total'] = sub_total
        return context


@csrf_exempt
def ajaxCartForm(request):
    if(request.method == 'POST'):
       
        cart, isCreated = Cart.objects.get_or_create(user = request.user, product_id = request.POST['product_id'])
    
        if isCreated:
            cart.quantity = request.POST['quantity']
        else:
            cart.quantity = cart.quantity + int(request.POST['quantity'])

        cart.save()



        response_data = {}
        data = {
                'cart':cart.id
                }
        response_data['result'] = 'Sucess'
        response_data['data'] = data

        return HttpResponse(json.dumps(response_data), content_type='application/json')
