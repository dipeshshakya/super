from django.db import models
from django.contrib.auth.models import User
from product.models import Product 
from account.models import Account


# Create your models here.
class Cart(models.Model):
	user = models.ForeignKey(User)
	product = models.ForeignKey(Product)
	quantity = models.IntegerField(default=1)
	created_on = models.DateTimeField(auto_now_add = True)
	modified_on = models.DateTimeField(auto_now_add = True)

