from django.contrib import admin
from .models import Category, Product, Location, ProductImage
from comments.models import Comments
from mptt.admin import MPTTModelAdmin
# Register your models here.

class ImageInline(admin.StackedInline):
    model = ProductImage

class CommentInline(admin.TabularInline):
    model = Comments
    extra = 0

class ProductAdmin(admin.ModelAdmin):
    inlines = [ImageInline,CommentInline,]
    list_display = ['name', 'price', 'qty', 'location', 'manufacturer', 'get_manufacture_email']
    prepopulated_fields = {'slug':('name',),}

class CategoryAdmin(MPTTModelAdmin):
    fields = ['name', 'parent']
    list_display = ('name', )


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Location)
