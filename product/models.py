from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
#from autoslug import AutoSlugField
# Create your models here.



DISTRICT_CHOICE = [
    ('KTM','Kathmandu'),
    ('LIT','Lalitpur'),
    ('BKT','Bhaktapur')
    ]


class Location(models.Model):
    district = models.CharField(choices=DISTRICT_CHOICE, max_length=10)
    village = models.CharField(max_length=100)
    ward_no = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    def __str__(self):
        return "%s %s %s" %(self.district, self.village, self.ward_no)


class Category(MPTTModel):
    name=models.CharField(max_length=200)
    slug=models.SlugField(unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    class MPTTMeta:
            order_insertion_by = ['name']

    def __str__(self):
            return self.name


##class Category(models.Model):
##    name = models.CharField(max_length=200)
##    slug = models.SlugField(unique=True)
##    parent = models.ForeignKey('self', blank=True, null=True)
##
##    def __str__(self):
##        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    slug = models.SlugField(unique=True)
    description = models.TextField()
    price = models.DecimalField(decimal_places=2, max_digits=11)
    qty = models.IntegerField()
    location = models.ForeignKey(Location)
    manufacturer = models.ForeignKey(User)
    category = TreeForeignKey('Category', null=True, blank=True, db_index=True)
##    category = models.ForeignKey(Category)
##    photo = models.ImageField(upload_to='Product_images',blank = True, null = True)
    def get_manufacture_email(self):
        return self.manufacturer.email
    get_manufacture_email.short_description = 'Email'


    def __str__(self):
        return self.name

    def get_avg_rating(self):
        from comments.models import Comments
        avg =Comments.average_rating(self)
        return avg    

class ProductImage(models.Model):
    photo = models.ImageField(upload_to='Product_images',blank = True, null = True)
    obj = models.ForeignKey(Product)
