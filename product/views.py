from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import Product, Category
from comments.models import Comments
# Create your views here.

class ProductListView(ListView):
    model = Product
    template_name = 'product/product_list.html'
    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        context['category_list'] = Category.objects.all()
    ##        context['category_list'] = Category.objects.filter(parent__isnull=True)
        return context

class ProductDetailView(DetailView):
    model = Product

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['category_list'] = Category.objects.all()
##        context['category_list'] = Category.objects.filter(parent__isnull=True)
        return context

from django.views.decorators.csrf import csrf_exempt
import json

from django.http import HttpResponse

@csrf_exempt
def ajaxCommentForm(request):

    if request.method == "POST":
        comment = Comments( user= request.user,
            product_id= request.POST['product_id'],
            comments=request.POST['comment'],
            rating= request.POST['rating']
        )
        comment.save()
        print comment
        response_data = {}
        time = comment.created_on.strftime(' %H:%M %p')
        date = comment.created_on.strftime('%Y-%b-%d')
        response_data = {"user_name": comment.user.username, 'comment_text': comment.comments, 'rating': comment.rating, 'comment_id': comment.id, 'time': time, 'date':date}

        return HttpResponse(json.dumps(response_data), content_type="application/json")