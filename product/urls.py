from django.conf.urls import include,url,patterns
from .views import ProductListView,ProductDetailView, ajaxCommentForm


urlpatterns = patterns(
    "",
    url(r'^$', ProductListView.as_view(),name='productlist'),
    url(r'^(?P<pk>\d+)/$',ProductDetailView.as_view(),name='productdetail'),
    url(r'^(?P<slug>[-\w]+)/$',ProductDetailView.as_view(),name='productdetail'),

    url(r'^ajax/comment/$', ajaxCommentForm )
)