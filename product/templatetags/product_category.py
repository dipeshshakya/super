from django import template

register = template.Library()

@register.inclusion_tag('product_category.html')
def show_category(category):

    return {'category_list':category}

def mul(price, qty):
    """multiple two values"""
    return price * qty

register.filter('mul', mul)