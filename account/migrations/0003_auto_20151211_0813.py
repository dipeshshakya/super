# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_fix_str'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='address',
            field=models.CharField(default=0, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='account',
            name='address2',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='gender',
            field=models.CharField(default='M', max_length=6, choices=[('M', 'Male'), ('F', 'Female'), ('O', 'Others')]),
        ),
        migrations.AddField(
            model_name='account',
            name='phone',
            field=models.CharField(max_length=10, unique=True, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='photo',
            field=models.ImageField(null=True, upload_to='profile', blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='rating',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='account',
            name='slug',
            field=models.SlugField(default=0, unique=True),
            preserve_default=False,
        ),
    ]
