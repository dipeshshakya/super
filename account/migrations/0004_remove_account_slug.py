# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20151211_0813'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='slug',
        ),
    ]
